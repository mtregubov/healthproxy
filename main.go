package main

import (
	"fmt"
	"log"
	"net/http"
	"time"
)

func checkhealth(w http.ResponseWriter, r *http.Request) {
	servicename := r.URL.Query().Get("sn")

	client := http.Client{
		Timeout: 5 * time.Second,
	}

	requrl := fmt.Sprintf("http://%s/api/system/health", servicename)
	log.Printf("Call http://%s/api/system/health", servicename)
	resp, err := client.Get(requrl)
	if err != nil || resp.StatusCode != 200 {
		log.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	defer resp.Body.Close()
	log.Println("OK")
	w.WriteHeader(http.StatusOK)
}
func main() {
	http.HandleFunc("/", checkhealth)
	log.Println("Starting on port 8080...")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
