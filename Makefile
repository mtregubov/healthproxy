build:
	go build -o healthproxy .

docker-build:
	docker build -t healthproxy .

run: build
	./healthproxy	
