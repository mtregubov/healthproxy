FROM golang:1.19-alpine as builder
WORKDIR /build
COPY . .
RUN go build -o /healthproxy main.go


FROM alpine:3
WORKDIR /app
COPY --from=builder healthproxy /app
ENTRYPOINT ["/app/healthproxy"]
